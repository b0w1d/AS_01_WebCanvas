Array.from(document.getElementsByClassName("tool-button")).forEach(u => {
  u.onclick = function() {
    let v = cur_tool();
    let flip = (s) => s.replace(/0.png/, "@.png").replace(/1.png/, "0.png").replace(/@/, "1");
    document.querySelector("#container").style.cursor = `url(${"assets/i" + u.src.match(/[^\/]+$/)[0]}) 4 12, default`;
    u.src = flip(u.src);
    v.src = flip(v.src);
  };
});

let cvs = document.querySelector("#cvs");
let ctx = cvs.getContext("2d");
cvs.width = window.innerWidth * 0.79;
cvs.height = window.innerHeight * 0.95;
let history = [cvs.toDataURL()];
let history_ptr = 0;

let px = 0;
let py = 0;
let is_mdown = false;
cvs.addEventListener("mouseup",   ( ) => { is_mdown = false; mup(); });
cvs.addEventListener("mouseout",  ( ) => is_mdown = false);
cvs.addEventListener("mousedown", (m) => { [is_mdown, px, py] = [true, m.offsetX, m.offsetY]; mdown(m); });
cvs.addEventListener("mousemove", (m) => is_mdown ? mmove(m) : null);

function set_text(e) {
  if (event.keyCode == 13) {
    ctx.fillStyle = document.getElementById("brush-color").value;
    ctx.font = `${document.getElementById("brush-size").value}px ${document.getElementById("text-font").value}`;
    ctx.fillText(e.value, e.offsetLeft, e.offsetTop);
    document.querySelector("#container").removeChild(e);
    save_state();
  }
}

function mdown(m) {
  if (cur_tool().name == "text") {
    let input = document.createElement("input");
    input.setAttribute('type', 'text');
    input.setAttribute('style', `position: absolute; left: ${px}px; top: ${py}px;`);
    input.setAttribute('onkeypress', 'set_text(this)');
    document.querySelector("#container").appendChild(input);
  }
}

var rainbow_hue = 0;

function mmove(m) {
  let img = new Image();
  img.src = history[history_ptr];

  ctx.lineCap = "round";
  ctx.strokeStyle = document.getElementById("brush-color").value;
  if (document.querySelector(".rainbow").src.match(/b.png$/)) {
    rainbow_hue += 1;
    rainbow_hue %= 360;
    ctx.strokeStyle = `hsl(${rainbow_hue}, 100%, 50%)`;
  }
  ctx.lineWidth = parseInt(document.getElementById("brush-size").value);
  ctx.globalCompositeOperation = "source-over";

  ctx.beginPath();

  if ("line" == cur_tool().name) {
    img.addEventListener("load", () => {
      ctx.clearRect(0, 0, cvs.width, cvs.height); 
      ctx.drawImage(img, 0, 0);
      ctx.moveTo(px, py);
      ctx.lineTo(m.x, m.y);
      ctx.closePath();
      ctx.stroke();
    });
  } else if (["brush", "eraser"].includes(cur_tool().name)) {
    if (cur_tool().name == "eraser") ctx.globalCompositeOperation = "destination-out";
    ctx.moveTo(px, py);
    ctx.lineTo(m.offsetX, m.offsetY);
    ctx.stroke();
    ctx.globalCompositeOperation = "source-over";
    [px, py] = [m.offsetX, m.offsetY];
  } else if ("circle" == cur_tool().name) {
    let cx = Math.floor((px + m.offsetX) / 2);
    let cy = Math.floor((py + m.offsetY) / 2);
    let dx = m.offsetX - px;
    let dy = m.offsetY - py;
    let r = Math.floor(Math.sqrt(dx * dx + dy * dy) / 2);
    img.addEventListener("load", () => {
      ctx.clearRect(0, 0, cvs.width, cvs.height); 
      ctx.drawImage(img, 0, 0);
      ctx.arc(cx, cy, r, 0, 2 * Math.PI);
      ctx.stroke();
    });
  } else if ("rect" == cur_tool().name) {
    let dx = m.offsetX - px;
    let dy = m.offsetY - py;
    img.addEventListener("load", () => {
      ctx.clearRect(0, 0, cvs.width, cvs.height); 
      ctx.drawImage(img, 0, 0);
      ctx.strokeRect(px, py, dx, dy);
    });
  } else if ("triangle" == cur_tool().name) {
    img.addEventListener("load", () => {
      ctx.clearRect(0, 0, cvs.width, cvs.height); 
      ctx.drawImage(img, 0, 0);
      ctx.moveTo(px, py);
      ctx.lineTo(m.x, m.y);
      ctx.lineTo(2 * px - m.x, m.y);
      ctx.closePath();
      ctx.stroke();
    });
  }
}

function mup() { if (["brush", "eraser", "rect", "triangle", "circle", "line"].includes(cur_tool().name)) save_state(); }

function cur_tool() {
  return Array.from(document.getElementsByClassName("tool-button"))
    .find(e => e.src.includes("1.png"));
}

function download() {
  let e = document.createElement("a");
  e.setAttribute("href", cvs.toDataURL("image/png"));
  e.setAttribute("download", "106000135.png");
  e.click();
}

function upload() {
  let e = document.createElement("input");
  e.setAttribute("type", "file");
  e.addEventListener("change", (event) => {
    let fr = new FileReader();
    fr.onload = function(e) {
      let img = new Image();
      img.src = e.target.result;
      img.addEventListener("load", () => {
        ctx.clearRect(0, 0, cvs.width, cvs.height); 
        ctx.drawImage(img, 0, 0);
        save_state();
      });
    };       
    fr.readAsDataURL(event.target.files[0]);
  }, false);
  e.click();
}

function save_state() {
  while (history_ptr + 1 < history.length) history.pop();
  history_ptr += 1;
  history.push(cvs.toDataURL());
}

function undo() {
  if (0 < history_ptr) {
    let img = new Image();
    img.src = history[history_ptr - 1];
    img.addEventListener("load", () => {
      ctx.clearRect(0, 0, cvs.width, cvs.height); 
      ctx.drawImage(img, 0, 0);
    });
    history_ptr -= 1;
  }
}

function redo() {
  if (history_ptr + 1 < history.length) {
    ctx.clearRect(0, 0, cvs.width, cvs.height); 
    let img = new Image();
    img.src = history[history_ptr + 1];
    img.addEventListener("load", () => {
      ctx.clearRect(0, 0, cvs.width, cvs.height); 
      ctx.drawImage(img, 0, 0);
    });
    history_ptr += 1;
  }
}

function rainbow() {
  let e = document.querySelector(".rainbow");
  e.src = e.src.replace(/a.png/, "@.png").replace(/b.png/, "a.png").replace(/@/, "b");
}

