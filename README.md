# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Rainbow mode                                  | 1~5%     | Y         |


---

### How to use 

There are two parts in the screen. The big rectangle that is blank in the beginning in the left is the canvas. The tool bar is in the right. 

The top row of the tool bar consists of the color panel, font-size controller, and the font-family controller, from left to right respectively. All the other icons in the tool bar is a button that has a unique function. Some are triggered immediately (such as the refresh button), while others, when clicked, will be recognized as the current tool on hand. The current tool is filled with color (except for the straight line function) as opposed to other unchosen tools being hollow. 

The second row consists of the brush, eraser, text generator, and refresh button from left to right. The brush and eraser works in a very similar way: having either of it clicked, you will be able to draw/erase the canvas by clicking the canvas and dragging the mouse. The text generator works a little differently: you'll need to click on a position you want the text to be generated at, and a textbox will appear - then type your text in the text box and hit enter - the text with the specified font settings and color will appear. The refresh button will clear the canvas and history (for undo and redo) when it's clicked.

The third row consists of the download function, upload function, undo, and redo. By clicking the download button, the image of the canvas is downloaded to the client side. By clicking the upload button, the client side will be asked to upload an image, and the canvas will be overwritten with that image. Undo and redo functions works the same way as most editors. 

The fourth row consists of four geometry shapes (the last one is a straight line). They basically work the same way: first click the canvas to decide the position of one end of the shape - then drag to the desired position of the other end of the shape - then leave the mouse up - the shape will appear.

The last function is the rainbow function, which is described below. 

### Function description

Toggle "rainbow mode" by clicking the rainbow icon at the bottom row. When the color of the icon is rainbow, the rainbow mode is on. When the rainbow mode is on, anything to be drawn to the canvas will have its color changed gradually.

### Gitlab page link

    https://b0w1d.gitlab.io/AS_01_WebCanvas

<style>
table th{
    width: 100%;
}
</style>
